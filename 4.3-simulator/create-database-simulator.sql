SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ANSI';

DROP DATABASE IF EXISTS `simulator`;

CREATE DATABASE `simulator`;

GRANT ALL ON simulator.* to cloud@`localhost`;
GRANT ALL ON simulator.* to cloud@`%`;

GRANT process ON *.* TO cloud@`localhost`;
GRANT process ON *.* TO cloud@`%`;

commit;
