#!/bin/bash -ex

source_dir=/vagrant/cloudstack-simulator

cd /tmp

# Dependencies
yum install git wget -y
rpm -i http://mirror.metrocast.net/fedora/epel/6/i386/epel-release-6-8.noarch.rpm || true
rpm -i http://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm || true
yum update -y
yum install \
  nc \
  tar \
  python-setuptools \
  java-devel \
  erlang \
  -y

# RabbitMQ
rpm -i http://www.rabbitmq.com/releases/rabbitmq-server/v3.2.3/rabbitmq-server-3.2.3-1.noarch.rpm
chkconfig --level 345 rabbitmq-server on
/etc/init.d/rabbitmq-server start

# set up ssh keys for access to bitbucket repo
# need this to grab CCP tar
mkdir -p /root/.ssh
chmod 700 /root/.ssh
cp $source_dir/known_hosts /root/.ssh/known_hosts
cp -p $source_dir/ssh-key /root/.ssh/id_rsa

# install CloudPlatform
wget http://67.20.167.112/cloudplatform/CloudPlatform-4.3.0.1-rhel6.4.tar.gz
tar -xzvf CloudPlatform-4.3.0.1-rhel6.4.tar.gz
cd CloudPlatform-4.3.0.1-rhel6.4 && ./install.sh -m -d

# setup CloudPlatform DBs
cp $source_dir/4.3-simulator/cloud-plugin-hypervisor-simulator-4.3.0.0.jar /usr/share/cloudstack-management/webapps/client/WEB-INF/lib
cp $source_dir/4.3-simulator/templates.simulator.sql /usr/share/cloudstack-management/setup
grep "INSERT INTO\|VALUES" /usr/share/cloudstack-management/setup/templates.simulator.sql >> /usr/share/cloudstack-management/setup/templates.sql
cloudstack-setup-databases cloud:cloud@127.0.0.1 --deploy-as=root
mysql -uroot < /usr/share/cloudstack-management/setup/create-database-simulator.sql
mysql -uroot < /usr/share/cloudstack-management/setup/create-schema-simulator.sql

# setup CloudPlatform
cloudstack-setup-management

# install maven
cd /usr/local
wget http://www.us.apache.org/dist/maven/maven-3/3.0.5/binaries/apache-maven-3.0.5-bin.tar.gz
tar -zxvf apache-maven-3.0.5-bin.tar.gz
export M2_HOME=/usr/local/apache-maven-3.0.5
export PATH=${M2_HOME}/bin:${PATH}

# we need cloudstack source to set up simulator infrastructure
cd /tmp
curl -L https://github.com/apache/cloudstack/archive/4.3.0-forward.tar.gz | tar -xz
cd cloudstack-4.3.0-forward
sed -i "s/sdist/install/g" tools/marvin/pom.xml
mvn -Pdeveloper -Dsimulator -DskipTests -Dmaven.install.skip=true install
cd tools/marvin
python setup.py install

# set up rabbit
mkdir -p /usr/share/cloudstack-management/webapps/client/WEB-INF/classes/META-INF/cloudstack/core/
cp $source_dir/spring-event-bus-context.xml /usr/share/cloudstack-management/webapps/client/WEB-INF/classes/META-INF/cloudstack/core/

# update db configuration
mysql -uroot cloud -e "update configuration set value = 'false' where name = 'router.version.check';"
mysql -uroot cloud -e "update configuration set value = '8096' where name = 'integration.api.port'"
mysql -uroot cloud -e "update service_offering set ram_size = 32;"
mysql -uroot cloud -e "update vm_template set enable_password = 1 where name like '%CentOS%';"
mysql -uroot cloud -e "update configuration set value = 0 where name like 'max.account.%';"
mysql -uroot cloud -e "insert into hypervisor_capabilities values (100,'100','Simulator','default',50,1,6,NULL,0,1);"
mysql -uroot cloud -e "update configuration set value = '10.100.2.1-10.100.2.8' where name = 'remote.access.vpn.client.iprange';"

# set up graylog
curl -L https://github.com/downloads/t0xa/gelfj/gelfj-1.0.1.jar -o /usr/share/cloudstack-management/webapps/client/WEB-INF/lib/gelfj-1.0.1.jar
curl -L https://json-simple.googlecode.com/files/json-simple-1.1.1.jar -o /usr/share/cloudstack-management/webapps/client/WEB-INF/lib/json-simple-1.1.1.jar
cp $source_dir/log4j-cloud.xml /usr/share/cloudstack-management/webapps/client/WEB-INF/classes/log4j-cloud.xml

# restart for settings to take affect
service cloudstack-management restart
while ! nc -vz localhost 8096; do sleep 10; done # Wait for CloudStack to start

# add simulator infrastructure
cd /tmp/cloudstack-4.3.0-forward/tools/devcloud
python ../marvin/marvin/deployDataCenter.py -i ../../setup/dev/advanced.cfg

# cloudmonkey
cd ~
easy_install pip
pip install cloudmonkey
cloudmonkey list zones > /dev/null
sed -i "s/8080/8096/" .cloudmonkey/config
sed -i "s/color = true/color = false/" .cloudmonkey/config

# update keys and create a template for Windows
root_admin_id=$(cloudmonkey list users | grep "^id =" | awk '{print $3};')
cloudmonkey update user id=$root_admin_id userapikey=F0Hrpezpz4D3RBrM6CBWadbhzwQMLESawX-yMzc5BCdmjMon3NtDhrwmJSB1IBl7qOrVIT4H39PTEJoDnN-4vA usersecretkey=uWpZUVnqQB4MLrS_pjHCRaGQjX62BTk_HU8uiPhEShsY7qGsrKKFBLlkTYpKsg1MzBJ4qWL0yJ7W7beemp-_Ng
#service_offering_id=$(cloudmonkey  list serviceofferings | grep "^id =" | awk '{print $3};' | head -1)
#template_id=$(cloudmonkey list templates templatefilter=featured | grep "^id =" | awk '{print $3};')
#zone_id=$(cloudmonkey list zones | grep "^id =" | awk '{print $3};')
#vm_id=$(cloudmonkey deploy virtualmachine name=windows5 zoneid=$zone_id templateid=$template_id serviceofferingid=$service_offering_id | grep "^id =" | awk '{print $3};')
#volume_id=$(cloudmonkey list volumes virtualmachineid=$vm_id | grep "^id =" | awk '{print $3};' | head -1)
#cloudmonkey stop virtualmachine id=$vm_id
#os_type_id=$(cloudmonkey list ostypes keyword=Windows | grep "^id =" | awk '{print $3};' | head -1)
#cloudmonkey create template volumeid=$volume_id name=WindowsOS displaytext=WindowsOS ostypeid=$os_type_id ispublic=true isfeatured=true
#cloudmonkey destroy virtualmachine id=$vm_id expunge=true

# Prepare for shutdown
service cloudstack-management stop

# Cleanup
rm -rf /tmp/*
yum clean all
find /var/log -type f | while read f; do echo -ne '' > $f; done;

# Reset Networking
sync
sed -i /HWADDR/d /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/sysconfig/networking-scripts/ifcfg-eth1
rm -f /etc/udev/rules.d/70-persistent-net.rules
